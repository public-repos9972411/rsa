
![Static Badge](https://img.shields.io/badge/build-jwt-brightgreen?style=flat-square&logo=%3Csvg%20role%3D%22img%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3Ctitle%3EPrivate%20Internet%20Access%3C%2Ftitle%3E%3Cpath%20d%3D%22M10.8078%205.7639a.812.812%200%201%200-.812.812.8115.8115%200%200%200%20.812-.812m3.196-.8115a.812.812%200%201%200%20.8114.8115.812.812%200%200%200-.8115-.8115m-.8757%202.1535a1.686%201.686%200%200%201-2.2571%200%20.2106.2106%200%200%200-.2855.3092%202.0645%202.0645%200%200%200%202.8298%200%20.2108.2108%200%201%200-.2866-.3092M11.9473%200C7.9939%200%204.789%203.2048%204.789%207.1582V8.543c0%20.0084.0016.0202.002.0293-.7826.1443-1.3781.8232-1.3888%201.6464a2.3927%202.3927%200%200%200-.33%201.2208v9.1777a2.4154%202.4154%200%200%200%201.7851%202.3281A1.7667%201.7667%200%200%200%206.4727%2024h2.0058a1.7627%201.7627%200%200%200%201.5762-.9668h3.6816c.3009.5943.9101.9678%201.5762.9668h2.0078a1.7718%201.7718%200%200%200%201.5899-1c1.1652-.1914%202.02-1.2%202.0175-2.3809v-9.1777a2.4275%202.4275%200%200%200-.3144-1.1973v-.0098c-.001-.7739-.5211-1.423-1.2305-1.625V7.1582C19.3828%203.2048%2016.1761%200%2012.2227%200Zm.0312%202.5586h.2149c2.4754%200%204.4824%202.005%204.4824%204.4805V8.543H15.668a1.6666%201.6666%200%200%200-1.1739.4804H9.5273a1.675%201.675%200%200%200-1.1836-.4804h-.8476V7.039c0-2.4754%202.007-4.4804%204.4824-4.4804zm.0254%209.4922v.0039a1.9739%201.9739%200%200%201%201.1055%203.6035.3126.3126%200%200%200-.1328.2988l.4863%203.2969a.307.307%200%200%201-.0703.2461.3137.3137%200%200%201-.2344.1074h-2.3164a.3097.3097%200%200%201-.3047-.3535l.4844-3.2969a.2934.2934%200%200%200-.129-.2949%201.975%201.975%200%200%201%20.8848-3.5976%202.176%202.176%200%200%201%20.2266-.0137z%22%2F%3E%3C%2Fsvg%3E&label=encryption)

# CREATE A JWT USING RSA in VBA

## Getting started

To use this code, follow these steps:

- In your VBA project, go to "Tools" > "References" and add a reference to "System.Security.Cryptography" from the .NET Framework.

To use the code, simply clone the repo into your local machine.

## Name
JWT CREATION USING RSA256 (in VBA)

## Description
This code enables one to create a jwt (json web token) by encoding the payload using RSA256 aligorithm.

## Installation

In this code:

- The SignPayload function takes the private key as an RSACryptoServiceProvider object instead of a string.
- The CreateJWT function receives the private key as an RSACryptoServiceProvider object from the ReadPrivateKeyFromFile function.
- The ReadPrivateKeyFromFile function reads a private key from a PEM file. It reads the entire contents of the PEM file into a string using System.IO.File.ReadAllText. It decodes the      
  PEM-encoded private key from Base64 format into a byte array using System.Convert.FromBase64String.
- It creates a new instance of RSACryptoServiceProvider. 
- It imports the private key bytes into the RSACryptoServiceProvider object using the ImportPkcs8PrivateKey method, specifying the key format as Pkcs8PrivateBlob.
- It returns the RSACryptoServiceProvider object containing the imported private key.

## Test and Deploy

As this example focuses on Asynchronous Algorithm, you will need a key pair, Private and Public keys. You can generate this using public available tools such as OpenSSL. For more details on how to achieve this vist this link [Key Pair Generartion]("https://www.purdue.edu/science/scienceit/ssh-keys-windows.html") where I give an example using keygen.

## Support
If you encounter a problem please reach out to me via Linkedin.

## Roadmap
I do not plan to relaease any future revisions of ths code. But feel free to submit any requests that you have.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Lwanga Nyambare

## License
This is an open source project.

## Project status
As mentioned previously, I do not intend to provide any revisions for this code. But if you feel there is something I missed please reach out in Linkedin. 
