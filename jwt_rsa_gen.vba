' Add a reference to 'System.Security.Cryptography' and 'System.Web.Extensions' from the .NET Framework

Public Function SignPayload(ByVal payload As String, ByVal privateKey As System.Security.Cryptography.RSACryptoServiceProvider) As String
    Dim encoder As New System.Text.UTF8Encoding
    Dim payloadBytes() As Byte = encoder.GetBytes(payload)
    
    Dim signatureBytes() As Byte = privateKey.SignData(payloadBytes, System.Security.Cryptography.HashAlgorithmName.SHA256, System.Security.Cryptography.RSASignaturePadding.Pkcs1)
    
    SignPayload = Convert.ToBase64String(signatureBytes)
End Function

Public Function CreateJWT(ByVal payload As String, ByVal privateKeyFile As String) As String
    Dim privateKey As System.Security.Cryptography.RSACryptoServiceProvider = ReadPrivateKeyFromFile(privateKeyFile)
    
    Dim header As String = "{""alg"":""RS256"",""typ"":""JWT""}"
    Dim encodedHeader As String = EncodeBase64(header)
    Dim encodedPayload As String = EncodeBase64(payload)
    
    Dim encodedSignature As String = SignPayload(encodedHeader & "." & encodedPayload, privateKey)
    
    CreateJWT = encodedHeader & "." & encodedPayload & "." & encodedSignature
End Function

Private Function EncodeBase64(ByVal input As String) As String
    Dim bytes() As Byte = System.Text.Encoding.UTF8.GetBytes(input)
    EncodeBase64 = Replace(System.Convert.ToBase64String(bytes), "=", "")
End Function

Private Function ReadPrivateKeyFromFile(ByVal filePath As String) As System.Security.Cryptography.RSACryptoServiceProvider
    Dim privateKeyText As String = System.IO.File.ReadAllText(filePath)
    Dim privateKeyBytes() As Byte = System.Convert.FromBase64String(privateKeyText)
    
    Dim rsa As New System.Security.Cryptography.RSACryptoServiceProvider
    rsa.ImportPkcs8PrivateKey(privateKeyBytes, System.Security.Cryptography.CngKeyBlobFormat.Pkcs8PrivateBlob)
    
    ReadPrivateKeyFromFile = rsa
End Function